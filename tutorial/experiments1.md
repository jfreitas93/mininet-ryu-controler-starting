# Testes simples para observação do comportamento do controlador em relação a mudanças feitas na rede em:

#### ```sudo mn --topo=linear,2 --controller remote,ip=127.0.0.1,port=6653```

    h1 --- s1 --- c0 --- s2 --- h2


### Teste 1: Verificar Conectividade Inicial

1. **Descrição:**
   - Inicie o Mininet com uma topologia linear de dois switches.
   - Inicie o controlador Ryu.
   - Execute um teste de conectividade inicial para garantir que os hosts na topologia possam se comunicar.

2. **Comandos Executados:**
   ```bash
   mininet> pingall
   ```
   
3. **Resultado Esperado:**
   - Os hosts `h1` e `h2` devem conseguir se comunicar sem problemas, pois a topologia inicial é linear e todos os links estão ativos.
    ```bash
    mininet> pingall
    *** Ping: testing ping reachability
    h1 -> h2 
    h2 -> h1 
    *** Results: 0% dropped (2/2 received)
    ```

---

### Teste 2: Desativar e Ativar Link entre `s1` e `s2`

1. **Descrição:**
   - Desative o link entre os switches `s1` e `s2`.
   - Execute novamente o teste de conectividade para verificar o impacto da queda do link.

2. **Comandos Executados:**
   ```bash
   mininet> link s1 s2 down
   mininet> pingall
   *** Ping: testing ping reachability
   h1 -> X 
   h2 -> X 
   *** Results: 100% dropped (0/2 received)

   ```

3. **Resultado Observado:**
   - O teste de conectividade (`pingall`) após a queda do link resulta em 100% de pacotes perdidos (`h1 -> X`, `h2 -> X`).
   - Isso ocorre porque o link entre `s1` e `s2` foi desativado, interrompendo a rota direta entre `h1` e `h2`.

   

---

### Teste 3: Restaurar Link entre `s1` e `s2`

1. **Descrição:**
   - Restaure o link entre os switches `s1` e `s2`.
   - Execute novamente o teste de conectividade para verificar se a comunicação é restaurada.

2. **Comandos Executados:**
   ```bash
   mininet> link s1 s2 up
   mininet> pingall
   *** Ping: testing ping reachability
   h1 -> h2 
   h2 -> h1 
   *** Results: 0% dropped (2/2 received)
   ```

3. **Resultado Observado:**
   - Após a restauração do link, o teste de conectividade (`pingall`) mostra que a comunicação entre `h1` e `h2` foi restaurada (`h1 -> h2`, `h2 -> h1`).

---

### Explicação:

- No teste inicial, todos os hosts podem se comunicar normalmente, pois a topologia está totalmente conectada.

- Quando o link entre `s1` e `s2` é desativado, ocorre a interrupção na comunicação entre `h1` e `h2`, pois não há mais um caminho direto entre esses hosts.

- Ao restaurar o link, a conectividade é restabelecida, e os hosts podem se comunicar novamente.

Esses testes ilustram a dinâmica de uma rede SDN controlada por Ryu, onde a queda e a restauração de links podem impactar diretamente na conectividade entre os hosts, demonstrando a flexibilidade e adaptabilidade desse tipo de rede.