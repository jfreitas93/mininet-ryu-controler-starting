# Configurações iniciais do Mininet e Ryu Controller

### Pré-requisitos

Certifique-se de ter o Mininet e o Ryu Controller instalados em seu sistema.

### Verificando as versões do Mininet
```bash
sudo mn --version
```

### Verificando a versão do Ryu Controller
```bash
ryu --version
```

### Passo 1: Criar a Topologia no Mininet

Abra um terminal (Terminal 1) e crie a topologia desejada usando o comando `sudo mn`. No exemplo abaixo, estamos criando uma topologia linear com 2 switches e um controlador remoto:

```bash
sudo mn --topo=linear,2 --controller remote,ip=127.0.0.1,port=6653
```

- `--topo=linear,2`: Cria uma topologia linear com 2 switches.
- `--controller remote,ip=127.0.0.1,port=6653`: Define o controlador como remoto na porta 6653.

### Passo 2: Iniciar o Ryu Controller

Abra outro terminal (Terminal 2) e inicie o Ryu Controller com o comando:

```bash
ryu-manager ryu-controller.py
```

### Passo 3: Explorar a Topologia no Mininet

De volta ao Terminal 1, você verá a saída indicando que a rede foi criada. Agora, você pode explorar a topologia usando comandos Mininet, como `pingall`, `net`, etc. Experimente alguns comandos para entender a topologia.

Exemplo de saída ao criar a topologia:

```bash
*** Creating network
*** Adding controller
*** Adding hosts:
h1 h2 
*** Adding switches:
s1 s2 
*** Adding links:
(h1, s1) (h2, s2) (s2, s1) 
*** Configuring hosts
h1 h2 
*** Starting controller
c0 
*** Starting 2 switches
s1 s2 ...
*** Starting CLI:
mininet> 
```

### Logs da construção da Rede no Mininet

- **Creating network:** Indica que está sendo criada uma rede.

- **Adding controller:** Um controlador está sendo adicionado à rede. Em redes SDN (Software-Defined Networking), o controlador desempenha um papel central na tomada de decisões sobre como os pacotes devem ser encaminhados na rede.

- **Adding hosts:** Dois hosts, identificados como h1 e h2, estão sendo adicionados à rede.

- **Adding switches:** Dois switches, identificados como s1 e s2, estão sendo adicionados à rede. Os switches são dispositivos de rede que encaminham pacotes de dados entre diferentes dispositivos na rede.

- **Adding links:** Estão sendo estabelecidas conexões (links) entre os hosts e os switches, bem como entre os próprios switches. Isso define como os dispositivos estão interconectados na rede.

- **Configuring hosts:** Configuração dos hosts h1 e h2 está ocorrendo. Isso pode envolver a atribuição de endereços IP, definição de rotas, etc.

- **Starting controller:** O controlador (c0) está sendo iniciado.

- **Starting switches:** Os switches s1 e s2 estão sendo iniciados.

- **Starting CLI:** A Interface de Linha de Comando (CLI) do Mininet está sendo iniciada. Isso permite interagir com a simulação, executar comandos, verificar o estado da rede, etc.

- **mininet>** Indica que agora você está na CLI do Mininet e pode começar a interagir com a rede simulada, por exemplo, para testar conectividade, verificar tabelas de roteamento, etc.


### Passo 4: Observar o Tráfego no Ryu Controller

Paralelamente, no Terminal 2, você verá a saída indicando que o Ryu Controller está carregando o aplicativo `ryu-controller.py`. Após a criação da topologia, o Ryu Controller receberá informações sobre os pacotes que estão sendo encaminhados pela rede.

Exemplo de saída ao iniciar o Ryu Controller:

```bash
loading app ryu-controller.py
loading app ryu.controller.ofp_handler
instantiating app ryu-controller.py of Switch
instantiating app ryu.controller.ofp_handler of OFPHandler
```
### Carregando Aplicativo Ryu Controller

- **loading app ryu-controller.py:** Indica que o aplicativo `ryu-controller.py` está sendo carregado.

- **loading app ryu.controller.ofp_handler:** Indica que o aplicativo `ryu.controller.ofp_handler` está sendo carregado.

- **instantiating app ryu-controller.py of Switch:** Sinaliza a instanciação do aplicativo `ryu-controller.py` para o Switch (dispositivo de rede).

- **instantiating app ryu.controller.ofp_handler of OFPHandler:** Sinaliza a instanciação do aplicativo `ryu.controller.ofp_handler` para o manipulador OFP (OpenFlow Protocol) chamado `OFPHandler`. O OpenFlow é um protocolo utilizado em redes SDN para a comunicação entre o controlador e os switches.

Essas mensagens indicam o carregamento correto e instanciação de aplicativos relacionados ao Ryu Controller, que é um framework para o desenvolvimento de controladores SDN usando Python. Após essas etapas, o controlador estará pronto para interagir com os switches na rede definida por software.

### Passo 5: Observar o Tráfego de Pacotes no Mininet

No Terminal 1, você verá mensagens indicando os pacotes recebidos pelos switches. Estas mensagens são simuladas para fins didáticos e mostram o movimento do tráfego na topologia.

Exemplo de saída ao observar o tráfego de pacotes em uma topologia linear com 2 switches criada no Passo 1:

```bash
Pacote recebido: switch 1, origem d2:1e:33:a4:0b:15, destino 33:33:00:00:00:16, porta de entrada 2
Pacote recebido: switch 2, origem 22:5d:23:e7:a1:fb, destino 33:33:00:00:00:16, porta de entrada 2
Pacote recebido: switch 1, origem 62:98:ff:89:90:ee, destino 33:33:ff:89:90:ee, porta de entrada 1
Pacote recebido: switch 2, origem 8e:f9:be:37:79:73, destino 33:33:ff:37:79:73, porta de entrada 1
...
```

### Significado do Tráfego de Pacotes

- **Pacote recebido**: Indica que um pacote foi recebido pelo switch.
- **Switch 1, Switch 2**: Identifica o switch específico em que o pacote foi recebido.
- **Origem, Destino**: Endereços MAC da origem e destino do pacote.
- **Porta de entrada**: A porta na qual o pacote entrou no switch.

Finalmente, você configurou uma topologia no Mininet, iniciou o Ryu Controller e observou o tráfego de pacotes na simulação. Este é o ponto de partida para experimentar e testar o trafego de pacotes em redes SDN usando essas ferramentas.