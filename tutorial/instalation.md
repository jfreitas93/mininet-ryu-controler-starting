# Guia de Instalação do Mininet e Ryu Controller

Este guia fornece instruções passo a passo para instalar o Mininet e o controlador Ryu em um ambiente Linux (Ubuntu). Este tutorial é destinado a alunos iniciantes que estão tendo seu primeiro contato com simulações de redes SDN e o sistema operacional Ubuntu.

## Passo 1: Atualização do Sistema

Abra o terminal e execute os seguintes comandos para garantir que seu sistema esteja atualizado:

```bash
sudo apt update
sudo apt upgrade -y
```

## Passo 2: Instalação do Mininet

### 2.1 Instalando o Git

O Git é uma ferramenta necessária para baixar o código-fonte do Mininet. Execute o seguinte comando:

```bash
sudo apt install git
```

### 2.2 Clonando o Repositório do Mininet

Clone o repositório do Mininet usando o Git:

```bash
git clone https://github.com/mininet/mininet
```

### 2.3 Executando o Script de Instalação

Navegue até o diretório do script de instalação e execute-o:

```bash
cd mininet/util
sudo ./install.sh -a
```

### 2.4 Atualizando em Caso de Erros

Se encontrar erros, tente corrigi-los atualizando novamente:

```bash
sudo apt-get update --fix-missing
```

## Passo 3: Instalação do Controlador Ryu

### 3.1 Instalando o pip (Gerenciador de Pacotes Python)

```bash
sudo apt install -y python3-pip
```

### 3.2 Clonando o Repositório do Ryu Controller

```bash
git clone https://github.com/osrg/ryu.git
```

### 3.3 Executando o Script de Instalação do Ryu

```bash
cd ryu
sudo python3 setup.py install
```

### 3.4 Atualizando o Ryu

Certifique-se de ter a versão mais recente do Ryu:

```bash
sudo pip3 install --upgrade ryu
```

## Observações Importantes

- Caso encontre erros relacionados a versões do eventlet ou outras dependências, verifique se está usando a versão correta do Python (Python 3) e atualize as bibliotecas conforme necessário.
Este guia oferece uma introdução básica à instalação do Mininet e do Ryu. Consulte a documentação oficial de cada projeto para obter informações mais detalhadas.
Lembre-se de que o terminal é uma ferramenta poderosa, e os comandos executados nele têm impacto no sistema. Certifique-se de entender cada passo antes de executar os comandos.