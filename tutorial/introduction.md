# Introdução

Este guia oferece uma introdução aos conceitos fundamentais de redes, fornecendo uma base para compreensão das tecnologias SDN (Redes Definidas por Software), controladores remotos, e a ferramenta de simulação de redes Mininet. Vamos explorar conceitos básicos, como switches, roteadores, máscaras de rede, endereços IP e pacotes, necessários para uma ambientação na simulação de redes.

## Switches

Os switches são dispositivos de rede que operam na camada de enlace (camada 2) do modelo OSI. Eles são responsáveis pelo encaminhamento eficiente de pacotes de dados entre dispositivos na mesma rede local. Os switches aprendem os endereços MAC dos dispositivos conectados e utilizam essa informação para encaminhar os pacotes apenas para o destino correto.

## Roteadores

Os roteadores operam na camada de rede (camada 3) do modelo OSI. Eles são responsáveis por encaminhar pacotes entre diferentes redes. Os roteadores tomam decisões de roteamento com base nos endereços IP dos pacotes.

## Máscaras de Rede

As máscaras de rede são utilizadas para dividir um endereço IP em duas partes: a parte de rede e a parte de host. Isso permite organizar os dispositivos em sub-redes, facilitando o gerenciamento e a comunicação eficiente.

Exemplo: Um endereço IP `192.168.1.1` com uma máscara de rede `255.255.255.0` indica que os primeiros 24 bits são destinados à identificação da rede e os últimos 8 bits são para identificar dispositivos naquela rede.

## Endereço IP

O endereço IP é um identificador único atribuído a cada dispositivo em uma rede TCP/IP. Existem versões diferentes de endereços IP, como IPv4 (por exemplo, `192.168.1.1`) e IPv6, que estão em transição devido ao esgotamento de endereços IPv4.

## Pacotes

Os pacotes são unidades de dados transmitidas em uma rede. Eles contêm informações como endereços de origem e destino, dados e informações de controle. Os switches e roteadores usam essas informações para encaminhar os pacotes corretamente pela rede.

# Redes SDN e Controladores Remotos

## Redes Definidas por Software (SDN)

Redes SDN são redes que utilizam a virtualização para centralizar o controle da rede. A separação do plano de controle e do plano de dados permite a programação dinâmica e flexível da rede, facilitando a adaptação a diferentes necessidades.

## Controladores SDN

Os controladores SDN são o cérebro por trás das redes SDN. Eles são responsáveis por gerenciar o tráfego de rede, definir políticas e fornecer uma interface para programação e automação da rede.

## RYU Controller

O RYU é um controlador SDN de código aberto. Ele permite o desenvolvimento de aplicativos para controlar o comportamento da rede SDN. O RYU é altamente modular e oferece suporte a protocolos como OpenFlow.

# Mininet - Simulação de Redes

O Mininet é uma ferramenta de emulação de rede que permite criar uma topologia de rede virtual usando máquinas virtuais. Ele é frequentemente usado para testar e desenvolver aplicativos para redes SDN. O Mininet fornece uma abordagem prática e econômica para experimentação e aprendizado em ambientes de rede complexos.