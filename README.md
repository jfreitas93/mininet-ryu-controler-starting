# Projeto de Introdução a Redes e Controladores SDN

---

### Objetivo do Projeto

O principal objetivo deste projeto é fornecer um tutorial didático e rápido para a introdução ao mundo de redes e controladores SDN (Software-Defined Networking). O material abrange desde a instalação até experimentos básicos, oferecendo uma experiência prática para estudantes e entusiastas interessados em explorar esse campo inovador.

### Conteúdo do Projeto

O projeto é composto pelos seguintes elementos:

1. **Arquivo do Controlador Ryu (`ryu-controller.py`):**
   - Este arquivo contém um script em Python que implementa a lógica básica de um controlador SDN usando o framework Ryu. Ele mostra como manipular eventos, lidar com pacotes e instalar regras de fluxo para direcionar o tráfego na rede.

2. **Documentação em tutorial/**
   - `introduction.md`: Oferece uma introdução ao contexto das redes e controladores SDN.
   - `instalation.md`: Guia passo a passo para a instalação rápida do ambiente necessário.
   - `start.md`: Instruções para iniciar o ambiente de teste.
   - `experiments1.md`: Experimentos básicos para aplicar e compreender os conceitos aprendidos.
   - `experiments2.md`: Continuação dos experimentos, explorando cenários mais avançados.

### Contribuições e Suporte

Este projeto foi desenvolvido como parte do programa de pós-graduação em Informática da UFPB - PPGI/UFPB. Se você tiver alguma contribuição, correção ou dúvida, sinta-se à vontade para abrir problemas (issues) ou enviar solicitações de pull (pull requests).

Esperamos que este projeto ajude a fornecer uma introdução prática e acessível ao fascinante mundo das redes e controladores SDN.

---